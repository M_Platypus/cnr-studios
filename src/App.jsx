// Main Imports
import React from 'react';
import { BrowserRouter } from 'react-router-dom';
import Routes from './Routes';
// components
import Navbar from './components/Navbar';

// Styling Stuff
import './style/style.css';

/*
Main Theme Colours ---
Background = navy - #001B44
Text  = orange - ##FF6300
Alt Text = light-green #9EEBCF
*/
const App = () => (
  <BrowserRouter>
    <main className="center bg-navy orange">
      <Navbar />
      <Routes />
    </main>
  </BrowserRouter>
);

export default App;
