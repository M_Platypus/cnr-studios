import React from 'react';
import { Route, Switch } from 'react-router-dom';
import { Home, Browse, Test } from './containers';

const Routes = () => (
  <Switch>
    <Route exact path="/" component={Home} />
    <Route path="/browse" component={Browse} />
    <Route path="/test" component={Test} />
  </Switch>
);

export default Routes;
