import React from 'react';
import {
  Image, Header, Card, Container,
} from 'semantic-ui-react';

import BeforeAfterSlider from 'react-before-after-slider';

import '../style/style.css';
import '../style/home.css';
import Logo from '../assets/CnR Studios.svg';
import image1 from '../assets/Image1.JPG';
import image2 from '../assets/Image2.JPG';

const Home = () => (
  <div className="wrapper">
    <section className="hero">
      <header className="hero__Content">
        <Image size="small" src={Logo} />
        <Header size="huge" color="teal" content="Redefining Photography" />
      </header>
    </section>
    <section className="team">
      <Header
        className="team__Title"
        size="huge"
        color="teal"
        content="Our Team"
      />
      <Card
        raised
        className="team__Card1 Cards"
        image="https://source.unsplash.com/2vbhN2Yjb3A/838x1257"
        header="Jefferson Clarke"
        color="orange"
        meta="The Brain"
        description="Lorem, ipsum dolor sit amet consectetur adipisicing elit. Excepturi eveniet sunt quo rem praesentium id facere quaerat deleniti ipsum modi."
      />
      <Card
        raised
        className="team__Card2 Cards"
        image="https://source.unsplash.com/rp0G_y4qXfk/838x1257"
        header="The Team"
        meta=" 👪 "
        description="Lorem, ipsum dolor sit amet consectetur adipisicing elit. Excepturi eveniet sunt quo rem praesentium id facere quaerat deleniti ipsum modi."
      />
      <Card
        raised
        className="team__Card3 Cards"
        image="https://source.unsplash.com/IPRFX7CVVoU/838x1257"
        header="William Rogers"
        meta="The Brawn"
        description="Lorem, ipsum dolor sit amet consectetur adipisicing elit. Excepturi eveniet sunt quo rem praesentium id facere quaerat deleniti ipsum modi."
      />
    </section>
    <section className="hero">
      <header className="hero__Content">
        <Image size="small" src={Logo} />
        <Header size="huge" color="teal" content="Redefining Photography" />
      </header>
    </section>
    <section className="about">
      <Header
        className="team__Title"
        size="huge"
        color="teal"
        content="What We Do"
      />

      <div style={{ maxWidth: '500px' }}>
        <BeforeAfterSlider
          before={image1}
          after={image2}
          width={640}
          height={480}
        />
      </div>
    </section>
  </div>
);

export default Home;
