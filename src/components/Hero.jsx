// Import react stuff
import React from 'react';
import {
  Container,
  Image,

} from 'semantic-ui-react';

// Import assets
import HeroImage from '../assets/009.jpg';
import Logo from '../assets/KS Logo.svg';

export default function Hero() {
  return (
    <div>
      <Container
        style={{
          backgroundImage: `url(${HeroImage})`,
          backgroundSize: 'cover',
          overflow: 'hidden',
        }}
        fluid
        ui
      >
        <div className="ba">
          <Image
            size="medium"
            centered
            src={Logo}
            className="ba h1 pb4 pt4 ma5"
          />
        </div>
      </Container>
    </div>
  );
}
