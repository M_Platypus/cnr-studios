import React from 'react';
import { Grid, Image } from 'semantic-ui-react';

const GridTest = () => (
  <Grid container columns={1}>
    <Grid.Column>
      <Image fluid src="https://react.semantic-ui.com/images/wireframe/image.png" />
    </Grid.Column>
    <Grid.Column>
      <Image src="https://react.semantic-ui.com/images/wireframe/image.png" />
    </Grid.Column>
    <Grid.Column>
      <Image src="https://react.semantic-ui.com/images/wireframe/image.png" />
    </Grid.Column>
  </Grid>
);

export default GridTest;
