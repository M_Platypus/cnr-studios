import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import {
  Container,
  Menu,
  Image,
  Icon,
} from 'semantic-ui-react';
import Logo from '../assets/CnR Studios.svg';

export default class Navbar extends Component {
    state = {
      activeItem: 'home',
    };

    handleItemClick = (e, { name }) => this.setState({ activeItem: name })

    render() {
      const { activeItem } = this.state;

      return (
        <Container fluid>
          <Menu stackable inverted>
            <Menu.Item as={Link} to="/" name="home" onClick={this.handleItemClick}>
              <Image size="tiny" src={Logo} />
            </Menu.Item>
            <Menu.Item as={Link} to="/" name="home" active={activeItem === 'home'} onClick={this.handleItemClick}>
              <Icon name="home" size="large" color="teal" />
                        Home
            </Menu.Item>
            <Menu.Item as={Link} to="/browse" name="about" active={activeItem === 'about'} onClick={this.handleItemClick}>
              <Icon name="camera retro" size="large" color="teal" />
                        Browse
            </Menu.Item>

            <Menu.Menu position="right">
            </Menu.Menu>
          </Menu>
        </Container>
      );
    }
}
